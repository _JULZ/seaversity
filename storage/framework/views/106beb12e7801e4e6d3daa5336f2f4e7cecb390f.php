<?php $__env->startSection('content'); ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-users"></i>
                  <?php echo e($data->name); ?>

                </p>
                <a href="<?php echo e(url('/admin/partners')); ?>" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
              <?php if($data->image): ?>
              <div class="row">
                <div class="col-md-12 text-center">
                  <img src="<?php echo e(asset('/uploads/'.$data->image)); ?>" width="40%">
                </div>
              </div>
              <?php else: ?>
              <div></div>
              <?php endif; ?>
          <form action="/admin/partner-update/<?php echo e($data->id); ?>" method="POST" enctype="multipart/form-data">
             <?php echo method_field('PUT'); ?>
                  <?php echo csrf_field(); ?>
                <div class="row mt-4">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="partner name *" name="name" required="" value="<?php echo e($data->name); ?>">
                         <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                        <input type="text" class="form-control" placeholder="link *" name="link" value="<?php echo e($data->link); ?>">
                         <?php $__errorArgs = ['about'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="about *" name="about"><?php echo e($data->about); ?></textarea>
                         <?php $__errorArgs = ['about'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                     <div class="col-md-12 ">
                       <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" name="img">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                      <button class="btn btn-outline-primary btn-sm float-right mt-2">Update</button>
                    </div>
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/partner_id.blade.php ENDPATH**/ ?>