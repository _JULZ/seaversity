<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | Works</title>

    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">
</head>
<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="/">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                <ul class="links">
                    <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a></li>
                    <li class="normal-link active"><a class="load-halvor" data-link='WORKS' href="/works">WORKS</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>

                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12 text-md-left text-center mt-30">

                        <!-- head-title -->
                        <div class="head-text">
                            <h2>EXPLORE OUR PROJECT</h2>
                            <div class="mt-10"></div>
                            <p>We’ve pushed ourselves to deliver the most forward-thinking digital experiences for our
                                clients. We fundamentally believe in supporting our people to create the future. More
                                than
                                any process or tool.</p>
                        </div>
                    </div>

                </div>
                <div id="headmove" class="lines-page">
                    <div data-depth="0.2" class="img-line">
                        <img src="<?php echo e(asset('img\lines.svg')); ?>" alt="">
                    </div>

                </div>
            </div>
        </section>
        <!-- End Header -->

        <section class="main-work between">
            <div class="container">
                <div class="row">
                    <!-- work item -->
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="work-item">
                            <a class="load-halvor" href="work-detail.html">
                                <div class="work-text">
                                    <h3 class="text-light"><?php echo e($data->name); ?></h3>
                                    <p class="text-light"><?php echo e($data->type); ?></p>
                                    <span>EXPLORE</span>
                                </div>
                                <div class="border-work"></div>
                                <div class="img-item">
                                   <img src="<?php echo e(asset('/uploads/'.$data->img)); ?>" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                

                </div>
            </div>
        </section>



          <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;sramos@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0912-345-6712</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="" method="POST">
                    <div class="form-group">
                     <input type="text" class="form-control"  placeholder="your name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control"  placeholder="your email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="your message *"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>
    </div>
    <!-- End Content -->
<script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
</body>

</html><?php /**PATH C:\xampp\htdocs\seaversity\resources\views//work.blade.php ENDPATH**/ ?>