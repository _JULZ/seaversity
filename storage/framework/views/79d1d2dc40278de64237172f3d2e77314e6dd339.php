<!DOCTYPE html>
<html lang="en">

<head>

<?php echo $__env->make('admin/layout/head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</head>


<body>
 <?php echo $__env->make('admin/layout/fb', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>	

  <?php echo $__env->make('admin/layout/sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('admin/layout/topbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <?php $__env->startSection('content'); ?>
<?php echo $__env->yieldSection(); ?>


<?php echo $__env->make('admin/layout/aside', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

<?php echo $__env->make('admin/layout/scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin/layout/footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</html><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/layout/admin_app.blade.php ENDPATH**/ ?>