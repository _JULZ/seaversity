<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | Services</title>

    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">
</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\seavclear.gif')); ?> " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="index.html">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>
                </div>

                <!-- navbar link -->
                <ul class="links">
                    <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link active"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>


                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
   
    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-page">
             <div class="lines-home-2">
                <div data-depth="0.2" class="img-line">
                   <div id="particles-js"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12 text-md-left text-center mt-30">

                        <!-- head-title -->
                        <div class="head-text">
                            <h2>OUR SERVICES</h2>
                            <div class="mt-10"></div>
                            <p>We have the expertise our client's capabilities through our Virtual Reality and E-Learning Programs and Services, which feature the following:</p>
                        </div>
                    </div>

                </div>
             
            </div>
        </section>
        <!-- End Header -->

        <section class="main-service between">
            <div class="container">
            
                </div>
                <div class="row">
                    <!-- service item -->
                    <div class="col-md-4 text-center">
                        <div class="button_su_service">
                            <span class="su_button_circle_service">
                            </span>
                            <div class="button_su_inner_service">
                                <span class="button_text_container_service">
                                    <div class="service-page-item">
                                        <div class="img-service-page">
                                            <img class="img-fluid" src="<?php echo e(asset('img\seav\icon\visualization-lg.png')); ?>" width="20%" alt="">
                                        </div>
                                        <div class="service-page-text">
                                            <h3>ENHANCED VISUALIZATION</h3>
                                            <p>Virtual Reality programs make use of a simulated environment where the users are immersed and are made to interact with 3D worlds.</p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- service item -->
                    <div class="col-md-4 text-center">
                        <div class="button_su_service">
                            <span class="su_button_circle_service">
                            </span>
                            <div class="button_su_inner_service">
                                <span class="button_text_container_service">
                                    <div class="service-page-item">
                                        <div class="img-service-page">
                                            <img class="img-fluid" src="<?php echo e(asset('img\seav\icon\tracking-lg.png')); ?>" alt="">
                                        </div>
                                        <div class="service-page-text">
                                            <h3>TRACKING AND MONITORING</h3>
                                            <p>To complement the experience of the VR programs, the users' learning progress is monitored and tracked.</p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- service item -->
                    <div class="col-md-4 text-center">
                        <div class="button_su_service">
                            <span class="su_button_circle_service">
                            </span>
                            <div class="button_su_inner_service">
                                <span class="button_text_container_service">
                                    <div class="service-page-item">
                                        <div class="img-service-page">
                                            <img class="img-fluid" src="<?php echo e(asset('img\seav\icon\data-lg.png')); ?>" alt="">
                                        </div>
                                        <div class="service-page-text">
                                            <h3>DATA ANALYTICS</h3>
                                            <p>From the assessment, necessary information are gathered and analyzed to determine and assess the effectiveness og the program this allows for continous improvement of the service/program.</p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>

                     <div class="col-md-4 text-center">
                        <div class="button_su_service">
                            <span class="su_button_circle_service">
                            </span>
                            <div class="button_su_inner_service">
                                <span class="button_text_container_service">
                                    <div class="service-page-item">
                                        <div class="img-service-page">
                                            <img class="img-fluid" src="<?php echo e(asset('img\seav\icon\visualization-lg.png')); ?>" width="20%" alt="">
                                        </div>
                                        <div class="service-page-text">
                                            <h3>Establishment of Quality Management System (QMS)</h3>
                                            <p>With the inputs of instructors, professors and pertinent members of the partner school/organization, Seaversity aids in establishing an effective and efficient Quality Management System (QMS). A QMS generally consists of policies, procedures and processes which are vital for the planning and execution of a school/company’s core businesses or offerings.</p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>

                     <div class="col-md-4 text-center">
                        <div class="button_su_service">
                            <span class="su_button_circle_service">
                            </span>
                            <div class="button_su_inner_service">
                                <span class="button_text_container_service">
                                    <div class="service-page-item">
                                        <div class="img-service-page">
                                            <img class="img-fluid" src="<?php echo e(asset('img\seav\icon\visualization-lg.png')); ?>" width="20%" alt="">
                                        </div>
                                        <div class="service-page-text">
                                            <h3>COURSE DEVELOPMENT</h3>
                                            <p>Seaversity offers development of maritime courses for its partner schools - including, but not limited to, crafting of syllabi, aligning course objectives to established standards, providing guidelines for instructors on how to achieve set course objectives, among others. Revision, updating and improvement of existing courses are also in-scope.</p>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
            </div>
        </section>

        <!-- End Work -->
        <!-- Start Footer -->
              <!-- Start Footer -->
   <section>

            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="<?php echo e(url('/mailer')); ?>">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>
        <!-- End Footer -->
    </div>

    <!-- End Content -->

    <script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
    <script src="<?php echo e(asset('js\particles.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin-js\part.js')); ?>"></script>
</body>

</html><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/services.blade.php ENDPATH**/ ?>