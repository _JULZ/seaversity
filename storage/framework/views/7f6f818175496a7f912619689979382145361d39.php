<!DOCTYPE html>
<html lang="en">

<head>

<?php echo $__env->make('layouts/head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</head>


<body>
<?php echo $__env->make('layouts/preload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


  <?php $__env->startSection('content'); ?>
<?php echo $__env->yieldSection(); ?>

</body>

<?php echo $__env->make('layouts/script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</html>
<?php /**PATH C:\xampp\htdocs\seaversity\resources\views/layouts/app1.blade.php ENDPATH**/ ?>