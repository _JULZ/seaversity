<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity - Maritime Technology Provider</title>

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css\app.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('fontawesome-free/css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">

</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\seavclear.gif')); ?> " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->
    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="/">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>
                </div>

                <!-- navbar link -->
                <ul class="links">
                    <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>

                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div id="halvor"> 
    <!-- Start Content -->
 <section class="header luxy-el" data-horizontal="1" data-speed-y="10">
     <div id="fb-root"></div>
      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="137982236900735"
         theme_color="#2fa5ff">
      </div>

        <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>

 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      
   <div class="lines-home-2">
                <div data-depth="0.2" class="img-line">
                   <div id="particles-js"></div>
                </div>
            </div>


    <div class="container">
     <div class="row">
                    <div class="col-md-6 col-12 text-md-left text-center mt-30">          
                        <!-- head-title -->
                        <div class="head-text">
                            <h2>THE CATALYST OF THE MARITIME FLEET
                            OF TOMORROW</h2>
                            <div class="mt-10"></div>
                            <p class="text-light">
                            >> We believe in pure vission implementation
                                <br>
                            >> Discover an easier way of learning</p>
                        </div>


                        <div class="button_su mt-10">
                            <span class="su_button_circle">
                            </span>
                            <a href="/products" class="button_su_inner">
                                <span class="button_text_container">
                                    OUR PRODUCTS
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-1 offset-md-5 text-right">
                        <!-- social media header -->
                        <div class="sosmed">
                            <ul>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.facebook.com/SeaversityPH/" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://twitter.com/SeaversityPH" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.instagram.com/seaversityPH/" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-instagram" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-youtube" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
              
            </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo e(asset('img\try5.png')); ?>" class="img-fluid" alt="...">
    </div>
    <div class="carousel-item">
        <div class="box">
            <img src="<?php echo e(asset('img\srg.png')); ?>" class="img-fluid" alt="...">
            <div class="text">   
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-left"> 
                </div>
                                    <div class="col-md-4">
                                                              <br>
                <br>
                <br>
                <br>
                                    <a href="https://lms.seaversity.com.ph/" target="_blank" class="btn btn-primary mt-70">
                                    Visit site
                                    </a>
                                    </div>
                    </div>       
               
             </div>
        </div>
      
    </div>
  </div>
 
  
</div>

        </section>

  <!-- product-->
         <section class="service between">
            <div class="container-fluid">
              <div class="row">
                 <div class="col-md-6">
                 </div>
                    <div class="col-md-6 text-center">
                        <!-- head-title -->
                        <div class="head-text">
                        <br>
                        <br>
                            <br>
                            <br>
                            <h2>PRODUCTS</h2>  
                            <div class="mt-10"></div>  
                            <p>Seaversity is a technology provider and enabler company, focusing on education and training systems seeking to provide highly competitive and innovative learning for the next generation maritime professionals and seafarers.</p>           
                        </div>
                       <div class="button_su_border mt-10">
                            <span class="su_button_circle_border">
                            </span>
                            <a href="/products" class="button_su_inner_border load-halvor">
                                <span class="button_text_container_border">
                                    SEE ALL
                                </span>
                            </a>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
              <div class="blog-detail bg-product"></div>
            </div>
        </section>
        <!--end product-->

        <!-- Start Service -->
        <section class="service between">
            <div id="headmove-2" class="lines">
                <div data-depth="0.2" class="img-line">
                    <img src="<?php echo e(asset('img\lines.svg')); ?>" alt="">
                </div>

            </div>
            <div class="container">

                <div class="row">
                    <div class="col-8">
                        <div class="service-text">
                            <h2>Services</h2>
                            <p class="text-primary">We specialize in implementing technological innovations into maritime education. We develop projects that make learning more convenient and efficient for aspiring seafarers.</p>
                        </div>
                    </div>

                    <div class="col-4 text-right">
                        <div class="button_su_border mt-10">
                            <span class="su_button_circle_border">
                            </span>
                            <a href="/services" class="button_su_inner_border load-halvor">
                                <span class="button_text_container_border">
                                    SEE ALL
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="service-item">
                            <div class="text-center">
                                <h3>Enhanced Visualization</h3>
                                 <img src="<?php echo e(asset('img\seav\icon\visualization-sm.png')); ?>" alt="">
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="service-item">
                            <div class="text-center">
                                <h3 class="mb-2">Tracking and Monitoring</h3>
                                 <img src="<?php echo e(asset('img\seav\icon\tracking-sm.png')); ?>" alt="">
                                <br>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-4">
                        <div class="service-item">
                            <div class="text-center">
                                <h3 class="mb-2">Data Analytics</h3>
                                 <img src="<?php echo e(asset('img\seav\icon\data-sm.png')); ?>" alt="">
                                <br>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="service-item">
                            <div class="text-center">
                                <h3 class="mb-2">Establishment of Quality Management System (QMS)</h3>
                                 <img src="<?php echo e(asset('img\seav\icon\visualization-sm.png')); ?>" alt="">
                                <br>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-4">
                        <div class="service-item">
                            <div class="text-center">
                                <h3 class="mb-2">Course Development</h3>
                                <img src="<?php echo e(asset('img\seav\icon\visualization-sm.png')); ?>" class="mt-3" alt="">
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Service -->

      

                <!-- Start Work -->
        <section class="work between">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <div class="title-main">
                            <h2>PORTFOLIO</h2>
                            <p>Our Selected projects.</p>
                        </div>
                    </div>

                    <div class="col-4 text-right">
                        <div class="button_su_border mt-10">
                            <span class="su_button_circle_border">
                            </span>
                            <a href="/works" class="button_su_inner_border">
                                <span class="button_text_container_border">
                                    SEE ALL
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="work-slider">
                    <div class="owl-carousel owl-theme">
                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <!-- work item -->
                        <div class="work-item">
                            <a class="load-halvor" href="/works/<?php echo e($data->id); ?>/<?php echo e(Str::slug($data->name)); ?>">
                                <div class="work-text">
                                    <h3 class="text-light"><?php echo e($data->name); ?></h3>
                                    <p class="text-light"><?php echo e($data->type); ?></p>
                                    <span class="text-light">EXPLORE</span>
                                </div>
                                <div class="border-work"></div>
                                <div class="img-item">
                                <img src="<?php echo e(asset('/uploads/'.$data->img)); ?>" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
            </div>
        </section>
        <!-- End Work -->

        <!-- Start About -->
        <section class="about between">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <div class="title-main">
                            <h2>ABOUT US</h2>
                        </div>
                    </div>
                 
                </div>

                <div class="row">
                    <div class="col-md-4">
                           <div class="about-text mt-30">
                                 <div class="box">
                            
                                   <img src="<?php echo e(asset('/img/milestone.jpg')); ?>" class="img-about">
                                    <div class="text">
                                         <h4 class="text-center"> <strong class="text-light">MILESTONES</strong></h4>
                                         <p class="card-text text-light text-center">Our company has reached two years recently! Check out the timeline of all our milestones and other significant events here.</p>
                                    <a href="/about">
                                      <div class="text-center text-light"><i class="fa fa-arrow-right"></i></div>
                                    </a>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-text mt-30">
                                 <div class="box">
                            
                                   <img src="<?php echo e(asset('/img/csr2.jpg')); ?>" class="img-about">
                                    <div class="text">
                                         <h4 class="text-center"> <strong class="text-light">CSR</strong></h4>
                                         <p class="card-text text-light text-center">Seaversity isn’t only about maritime education. We also want to contribute to society, helping people less fortunate than us</p>
                                    <a href="/about">
                                      <div class="text-center text-light"><i class="fa fa-arrow-right"></i></div>
                                    </a>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-text mt-30">
                                 <div class="box">
                            
                                   <img src="<?php echo e(asset('/img/out_team.jpg')); ?>" class="img-about">
                                    <div class="text">
                                         <h4 class="text-center"> <strong class="text-light">OUR LEADS</strong></h4>
                                         <p class="card-text text-light text-center">Meet the visionaries of Seaversity who made the company to what it is today, and continues to bring innovations in the maritime industry.</p>
                                    <a href="/about">
                                      <div class="text-center text-light"><i class="fa fa-arrow-right"></i></div>
                                    </a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End About -->

       <!--  team -->
         <section class="between team">
            <div class="container-fluid">
                <div class="team-slider">
              
                    <div class="owl-carousel owl-theme">

                        <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="team-item">
                             <a class="load-halvor" href="/partner/<?php echo e($partner->id); ?>/<?php echo e(Str::slug($partner->name)); ?>">
                            <div class="bg-hover-team"></div>
                            <div class="team-text">
                                 <ul>
                                    <li><h3><?php echo e($partner->name); ?></h3></li>
                                    <li><p><?php echo e($partner->link); ?></p></li>
                                </ul>
 
                            </div>
                            <div class="img-item">
                                 <img src="<?php echo e(asset('/uploads/'.$partner->image)); ?>" class="img-fluid" alt="">
                            </div>
                        </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>      
            </div>
        </section>
       <!--  /team -->

        <!-- Start News -->
        <section class="news between">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <div class="title-main">
                            <h2>News</h2>
                         
                        </div>
                    </div>

                    <div class="col-4 text-right">
                        <div class="button_su_border mt-10">
                            <span class="su_button_circle_border">
                            </span>
                            <a href="/news" class="button_su_inner_border load-halvor">
                                <span class="button_text_container_border">
                                    SEE ALL
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="news-slider">
                    <div class="mt-30"></div>
                    <div class="owl-carousel owl-theme">

                        <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="news-item">
                            <div class="bg-hover"></div>
                            <a class="load-halvor" href="/news/<?php echo e($news->id); ?>/<?php echo e(Str::slug($news->title)); ?>">
                                <div class="news-text">
                                    <p class="text-light"><?php echo e(date('F d, o',strtotime($news->date_created))); ?></p>
                                    <h3><?php echo e($news->title); ?></h3>
                                    <img src="<?php echo e(asset('img\arrrow.png')); ?>" alt="">

                                </div>
                                <div class="img-item">
                                <img src="<?php echo e(asset('/uploads/'.$news->image)); ?>" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- End News -->

        <!-- Start Footer -->
   <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="<?php echo e(url('/mailer')); ?>">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>
        <!-- End Footer -->
    </div>

    <!-- End Content -->

    <script type="text/javascript" src="<?php echo e(asset('js\app.js')); ?>"></script>
    <script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
     <script src="<?php echo e(asset('js\particles.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin-js\part.js')); ?>"></script>
</body>

</html><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/home.blade.php ENDPATH**/ ?>