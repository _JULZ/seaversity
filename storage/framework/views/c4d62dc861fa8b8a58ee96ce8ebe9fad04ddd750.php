<?php $__env->startSection('content'); ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="card">
        <div class="row">
          <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-vr-cardboard"></i>
                  Services
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-service">
                <i class="fas fa-plus">&nbsp;</i>Add Services
                </button>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
               <table class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Service name</th>
                  <th>Description</th>
                  <th>Type</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($dt->name); ?></td>
                  <td><?php echo e($dt->description); ?></td>
                  <td><?php echo e($dt->type); ?></td>
                  <td class="text-center">
                    <a href="<?php echo e(url('admin/service/').'/'.$dt->id); ?>" class="btn btn-primary btn-sm"><i class="fa fa-pen"></i></a>
                  </td>
                </tr>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tfoot>
              </table>
              <div class="mt-3 float-right">  <?php echo e($data->links()); ?></div>
             </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="add-service">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-vr-cardboard">&nbsp;</i>Add Service</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
           
          <div class="modal-body bg-white">
              <form action="/admin/add_service" method="POST">
                <?php echo csrf_field(); ?>
             <div class="row">
              <div class="col-md-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="service name *" name="name" required="">
                         <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <select class="custom-select"  name="type">
                          <option>select type</option>
                          <option value="VR">VR</option>
                          <option value="e-Learning">e-Learning</option>
                        </select>
                        <?php $__errorArgs = ['service'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <!-- textarea -->
                      <div class="form-group">
                        <textarea class="form-control" rows="8" placeholder="description *" name="description"></textarea>
                        <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input type="hidden" name="status" value="1">
                      </div>
                    </div>
                  </div>
            </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div> 
</section>
</div>
</div>
<!-- ./wrapper --> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin/layout/admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/services.blade.php ENDPATH**/ ?>