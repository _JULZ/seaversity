<?php $__env->startSection('content'); ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
           
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-vr-cardboard"></i>
                  <?php echo e($data->name); ?>

                </p>
                <a href="<?php echo e(url('/admin/services')); ?>" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
          <form action="/admin/service-update/<?php echo e($data->id); ?>" method="POST">
             <?php echo method_field('PUT'); ?>
                  <?php echo csrf_field(); ?>
                <div class="row">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="service name *" name="name" required="" value="<?php echo e($data->name); ?>">
                         <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                        <select class="custom-select"  name="type">
                        <?php if($data->type == 'e-Learning'): ?>
                        <option value="<?php echo e($data->type); ?>">e-Learning</option>
                        <option value="VR">VR</option>
                        <?php endif; ?>
                        <?php if($data->type == 'VR'): ?>
                        <option value="<?php echo e($data->type); ?>">VR</option>
                        <option value="e-Learning">e-Learning</option>
                        <?php endif; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="service description" name="description"><?php echo e($data->description); ?></textarea>
                         <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                     <div class="col-md-12 ">
                      <button class="btn btn-outline-primary btn-sm float-right">Update</button>
                    </div>
                  </div>
                   
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/service_id.blade.php ENDPATH**/ ?>