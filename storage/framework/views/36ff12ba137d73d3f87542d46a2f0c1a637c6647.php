<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | Products</title>
    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">
</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\seavclear.gif')); ?> " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="index.html">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                 <ul class="links">
                    <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link active"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>


                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
               <!-- Start Work -->
        <section class="work between mt-50">

            <div class="container">
              <div class="row">
                    <div class="col-md-12 col-sm-6">
                        <div class="head-text">
                            <h3 class="text-primary text-center">Extended Reality (XR)</h3>
                            <div class="mt-10 text-center">
                            </div>
                           
                        </div>
                    </div>
                 
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <img src="<?php echo e(asset('img\gears.png')); ?>" width="100%" alt="">
                        </div>
                        <div class="col-md-12">
                            <p>We are always looking to utilize the latest technologies to be able to provide greater convenience and efficiency in maritime education when possible. The use of extended reality allows students to better visualize objects, locations, and activities, without the actual need for the physical machinery or location.</p>
                    </div>     
            </div>
        </section>
        <!-- End Header -->

        <!-- Start Content -->
        <section class="work-page between">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12  text-center">
                        <div class="work-desc-text">
                            <h3 class="text-primary">Learning Management System</h3>
                            <p>The Seaversity LMS is a comprehensive system to provide competency management solutions for the maritime industry. Our systems are primarily authored with the top development platforms and e-learning tools to ensure fully interactive and gamified technology. The content is thoroughly developed by Maritime SMEs all over the Philippines so that each training may be cascaded, monitored, and validated anywhere in the world.</p>
                        </div>
                            <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="/lms" class="button_su_inner">
                                    <span class="button_text_container">
                                        LEARN MORE &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                    </div>
                </div>
                  <div class="row mt-100">
                       <div class="col-md-6 col-sm-12 text-md-left text-center">
                        <div class="work-desc-text">
                            <h3 class="text-primary">Assessment</h3>
                            <p>After the user gets to familiarize themselves with the parts, definitions, and functions within the VR simulations, they may then test themselves with the assessments, also developed and playable in VR.</p>
                        </div>  
                    </div>  
                        <div class="col-md-6 col-sm-12">
                          <iframe width="530" height="310" src="https://www.youtube.com/embed/CuYiXnhKLPQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>

<br>
<br>
<br>
<br>
            <div class="container mt-150">
                <div class="row text-md-right">
                    <div class="col-md-6 col-sm-12 mt-30">
                
                   <iframe width="530" height="310" src="https://www.youtube.com/embed/-1ix4LWBTOM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                      </div>
                      
                       <div class="col-md-6 col-sm-12">
                           <div class="work-detail-text">
                            <h3 class="text-primary text-md-left">Augmented Reality Simulations</h3>
                            <p class="text-left">Seaversity has developed many AR applications, allowing the close viewing and study of the parts of different ship machinery. Some have animations simulating how the part moves in real time, while some can be disassembled and assembled to view how the parts are constructed.
                              <br>
           > Pumps - Study the difference between different kinds of ship pumps
           <br>
           > Valves - Learn the functions of various types of ship valves
           <br>
           > Multipurpose Controller - Disassemble the multipurpose controller for better study
           <br>
           > Fuel Pump - Find out the fuel pump’s parts and function</p>
                        </div>
                      </div>

            </div>
        </div>
    <br>
 <div class="container mt-10">
                <div class="row text-center">
                       <div class="col-md-12 col-sm-6">
                        <div class="head-text mt-90">
                            <h4 class="text-primary">VIRTUAL REALITY SIMULATIONS</h4>   
                            <p>
                              As one of the first Filipino maritime VR adopters and digitizers, we develop vessel-specific familiarization and interactive simulations to ensure immersive training experience related to deck department, engineering requirements, among others; providing walk-throughs of actual ships to give trainees a full onsite view of the vessels.
                            </p>                 
                        </div>
                      </div>
                      <br> 

                        <div class="col-md-3 text-center mt-2">
                          <a href="/deck">
                        <div class="team-item">
                            <div class="bg-hover-team"></div>
                            <div class="img-item">
                                <img src="<?php echo e(asset('img\icon\deckVR-lg.png')); ?> " width="30%" alt="">
                            </div>
                                </a>
                             <h5 class="text-light mt-10">Deck Simulations</h5>
                            </div>
                        </div>
                      <div class="col-md-3 col-sm-6 mt-2">
                          <a href="/engine">
                        <div class="team-item">
                            <div class="bg-hover-team"></div>
                            <div class="img-item">
                                <img src="<?php echo e(asset('img\icon\\engineVR-lg.png')); ?> " width="30%" alt="">
                            </div>
                                </a>
                             <h5 class="text-light mt-10">Engine Simulations</h5>
                            </div>
                      </div>
                      <div class="col-md-3 col-sm-6 mt-2">
                         <a href="/vr-lfa">
                        <div class="team-item">
                            <div class="bg-hover-team"></div>
                            <div class="img-item">
                                <img src="<?php echo e(asset('img\icon\safetytraining-lg.png')); ?> " width="30%" alt="">
                            </div>
                                </a>
                             <h5 class="text-light mt-10">Life Saving Appliances</h5>
                            </div>
                      </div>
                      <div class="col-md-3 col-sm-6 mt-2">
                        <a href="/vr-ffe">
                        <div class="team-item">
                            <div class="bg-hover-team"></div>
                            <div class="img-item">
                                <img src="<?php echo e(asset('img\icon\deckVR-lg.png')); ?> " width="30%" alt="">
                            </div>
                                </a>
                             <h5 class="text-light mt-10">Firefighting Equipment</h5>
                            </div>
                      </div>
            </div>
        </div>
        </section>
        <!-- End Content -->


        <!-- Start Footer -->
   <section>
            <div class="container mt-90">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="<?php echo e(url('/mailer')); ?>">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="your name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="your email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="your message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>  
        <!-- End Footer -->

    </div>
    <!-- End Content -->

    <script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\seaversity\resources\views/product.blade.php ENDPATH**/ ?>