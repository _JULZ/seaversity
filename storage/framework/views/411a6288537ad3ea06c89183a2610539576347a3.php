<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | AX</title>
    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">
</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="index.html">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                <ul class="links">
                         <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link active"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>

                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-detail-work luxy-el" data-horizontal="1" data-speed-y="10">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- head-title -->
                        <div class="head-text">
                            <img src="<?php echo e(asset('/img/ax-min.png')); ?>">
                        </div>
                    </div>
                </div>
              
                <!-- main background-->
                <div class="work-detail bg-work-2"></div>
                <div id="headmove" class="lines">
                        <div data-depth="0.2" class="img-line">
                            <img src="<?php echo e(asset('img\lines.svg')); ?>" alt="">
                        </div>
                    </div>
            </div>
        </section>
        <!-- End Header -->

        <!-- Start Content -->
        <section class="work-page between">
                <div class="mouse-down page"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 text-md-left text-center">
                        <iframe width="1110" height="620" src="https://www.youtube.com/embed/nZsoR1v1DY4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                </div>


                <div class="row">
                    <div class="col-md-6">
                         <div class="head-text mt-90">
                            <h2>SEAVERSITY AX</h2>                          
                        </div>
                          <p>An online system that tackles the maritime industry's woes. It addresses the 'Backer system' that reduces the reliability of the hiring process. It addresses the 'Backer system' that reduces the reliability of the hiring process. Seafarers are short-changed for the value which they paid for in terms of effective assessment.</p>
                    </div>
                    <div class="col-md-6">
                        <div class="img-work-detail">
                               <div class="card">
                                <div class="card-header bg-primary text-center">
                                   <h4> <strong class="text-light">BENEFITS</strong></h4>
                                </div>
                            <div class="card-body">
                                <h5 class="card-title"><strong>CUT DOWN EXPENSES</strong></h5>
                                    <p class="card-text text-dark">Reduce man power and work hours utilized for interviews and initial meetings</p>
                                <h5 class="card-title"><strong>ELIMINATE INCONSISTENCIES</strong></h5>
                                     <p class="card-text text-dark">Utilize a series of curated examinations with a set standard</p>
                                <h5 class="card-title"><strong>STREAMLINE HR PROCESSES</strong></h5>
                                     <p class="card-text text-dark">Connect easily with top-performing users from Step 1 until hiring</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <br>
    <br>

     <div class="container mt-100">
                <div class="row text-center">
                    <div class="col-md-12 col-sm-12 text-center ">
                    <!--     <br>
                        <br>
                        <br>
                        <br> -->
                     <img src="<?php echo e(asset('/img/seav/laptop.png')); ?>">
                     <br>
                          <div class="button_su">
                                <span class="su_button_circle">
                                </span>
                                <a href="https://ax.seaversity.com.ph/" target="_blank" class="button_su_inner">
                                    <span class="button_text_container">
                                        VISIT SITE &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                      </div>

            </div>
        </div>

 <div class="container mt-100">
  <div class="row text-center">
  <div class="col-md-12 col-sm-12 table-responsive">
  <table class="table table-borderless">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col" class="textcenter bg-secondary"><img src="<?php echo e(asset('/img/seav/pro2.png')); ?>"></th>
      <th scope="col" class="textcenter bg-primary"><img src="<?php echo e(asset('/img/seav/lite2.png')); ?>"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      <td class="bg-light">Offers hiring, retention, and promotion assessments for officers</td>
      <td class="bg-light">Offers basic assessments to training cadets for streamlined evaluation</td>
    </tr>
    <tr>
      <th scope="row" class="text-light"><h5 class="text-primary">ONLINE THEORETICAL ASSESSMENT</h5>
        <p>Web-based examinations curated for initial assessments</p>
      </th>
      <td class="bg-secondary"><i class="fa fa-bookmark fa-2x mt-4"></i></td>
      <td class="bg-primary"><i class="fa fa-bookmark fa-2x mt-4 text-light"></i></td>
    </tr>
    <tr>
      <th scope="row" class="text-light"><h5 class="text-primary">FAST AND COMPREHENSIVE RESULTS</h5>
        <p>Instant statistics provided for evaluator analytics</p>
      </th>
       <td class="bg-secondary"><i class="fa fa-bookmark fa-2x mt-4"></i></td>
      <td class="bg-primary"><i class="fa fa-bookmark fa-2x mt-4 text-light"></i></td>
    </tr>
      <tr>
      <th scope="row" class="text-light"><h5 class="text-primary">PSYCHOMETRIC BEHAVIORAL AND PERSONALITY ASSESSMENT</h5>
        <p>Holistic assessment of desired qualifications</p>
      </th>
       <td class="bg-secondary"><i class="fa fa-bookmark fa-2x mt-4"></i></td>
      <td class="bg-primary"><i class="fa fa-minus-square fa-2x mt-4 text-light"></i></td>
    </tr>
      <tr>
      <th scope="row" class="text-light"><h5 class="text-primary">PRACTICAL ASSESSMENT WITH ADVANCED SIMULATIONS</h5>
        <p>Hands-on award-winning simulators for maximum training</p>
      </th>
      <td class="bg-secondary"><i class="fa fa-bookmark fa-2x mt-4"></i></td>
      <td class="bg-primary"><i class="fa fa-minus-square fa-2x mt-4 text-light"></i></td>
    </tr>
    <tr>
      <th scope="row" class="text-light"><h5 class="text-primary">DIRECT PROMOTION AND RECOMMENDATION</h5>
        <p>Hassle-free link to desired applicants</p>
      </th>
      <td class="bg-secondary"><i class="fa fa-bookmark fa-2x mt-4"></i></td>
      <td class="bg-primary"><i class="fa fa-minus-square fa-2x mt-4 text-light"></i></td>
    </tr>
  </tbody>
</table>
                      </div>
            </div>
        </div>
        </section>
        <!-- End Content -->


        <!-- Start Footer -->
   <section>
            <div class="container mt-50">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="<?php echo e(url('/mailer')); ?>">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="your name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="your email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="your message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>  
        <!-- End Footer -->
    </div>
    <!-- End Content -->

    <script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\seaversity\resources\views/ax.blade.php ENDPATH**/ ?>