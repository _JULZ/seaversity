<?php $__env->startSection('content'); ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="row">
            <div class="col-md-12">
             <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-newspaper"></i>
                  News
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-news">
                <i class="fas fa-plus">&nbsp;</i>Add News
                </button>
              </div>
              </div>
            </div>
          </div>
        <div class="card-body">
          <table  class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Content</th>
                  <th>Image</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($dt->title); ?></td>
                  <td><?php echo e($dt->date_created); ?></td>
                  <td><?php echo e($dt->content); ?></td>
                  <?php if(!($dt->image)): ?>
                  <td></td>
                  <?php else: ?>
                  <td><img src="<?php echo e(asset('/uploads/'.$dt->image)); ?>" width="100 px"></td> 
                  <?php endif; ?>
                   <td class="text-center">
                    <?php if($dt->status == '1'): ?>
                    <button class="btn btn-secondary btn-sm mt-2 news_off" data-ids="<?php echo e($dt->id); ?>" data-names="<?php echo e($dt->name); ?>">OFF</button>
                    <?php else: ?>
                    <button class="btn btn-primary btn-sm mt-2 news_on" data-ids="<?php echo e($dt->id); ?>" data-names="<?php echo e($dt->name); ?>">ON</button>
                  <?php endif; ?>
                  <br>
                    <a href="<?php echo e(url('admin/news/').'/'.$dt->id); ?>" class="btn btn-primary btn-sm mt-2"><i class="fa fa-pen"></i></a>
                    <button class="btn btn-danger btn-sm destroy_news mt-2"   type="button" data-ids="<?php echo e($dt->id); ?>" data-names="<?php echo e($dt->title); ?>"><i class="fas fa-trash"></i></button>  
                </td>
                </tr>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tfoot>
             
            </table>
            <div class="mt-3 float-right">  <?php echo e($data->links()); ?></div>
      </div>
             
        </div>
         <!--modal-->
      <div class="modal fade" id="add-news">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-newspaper">&nbsp;</i>Add News</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
          <div class="modal-body bg-white">
              <form action="<?php echo e(url('/admin/add_news')); ?>" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
             <div class="row">
              <div class="col-md-6 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="title *" name="title" required="">
                         <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <input type="text" class="form-control" name="date_created" required="">
                         <?php $__errorArgs = ['date_created'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
               </div>
              </div>
              <!-- second col-->
              <div class="row">
              <div class="col-md-12 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                         <textarea class="form-control" rows="6" placeholder="content *" name="content" ></textarea>
                         <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                         <select class="custom-select"  name="type">
                          <option value="News">News</option>
                          <option value="Milestone">Milestone</option>
                        </select>
                         <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                      <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" required="" name="image">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    </div>
              </div>      
              </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </form>
          </div>
        </div>
          <!-- /.modal-content -->
        </div>
      </div>
      <!-- /.container-fluid -->
    </section> 
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/news.blade.php ENDPATH**/ ?>