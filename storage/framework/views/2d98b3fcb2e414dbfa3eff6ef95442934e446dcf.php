<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | About</title>
    <link rel="stylesheet" href="<?php echo e(asset('css\plugin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css\responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('fonts\font-awesome\css\all.css')); ?>">
</head>
<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="<?php echo e(asset('img\seavclear.gif')); ?> " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="/">
                        <img src="<?php echo e(asset('img\logo.svg')); ?>" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                <ul class="links">
                        <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link active"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>

                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-page">
              <div class="lines-home-2">
                <div data-depth="0.2" class="img-line">
                   <div id="particles-js"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 text-md-left text-center">
                        <!-- head-title -->
                        <div class="head-text">
                            <h2>About Seaversity</h2>
                            <div class="mt-10"></div>
                            <p>Seaversity was established in 2017, by visionaries aiming for a huge advancement in the maritime industry. The team and its members believe that this can be achieved through technologically-powered programs, platforms and services.

                                We pride ourselves not only in our members' vast experience in seafaring and maritime training, but also in the unparalleled capabilities of our IT experts. The company may be young; but these capabilities, combined with our passion and perseverance, are enough to place us alongside other established companies offering the same services as ours.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="head-text">
                            <h2>Vision</h2>
                            <div class="mt-10"></div>
                            <p>Become the catalyst of Philippine Maritime Education and Training to global competitiveness through world-class innovations and technological solutions.</p>
                        </div>
                         <div class="head-text">
                            <h2>Mission</h2>
                            <div class="mt-10"></div>
                            <p>Provide the innovations of the learning system for the next generation of maritime professionals.</p>
                        </div>
                    </div>


                 <div class="col-md-12">
                        <!-- head-title -->
                        <div class="head-text">
                            <h2>Our Goals</h2>
                            <div class="mt-10"></div>
                            <p>We are driven primarily by our objective to enhance the equality of maritime education and training with the use of futuristic technology. We believe in capability building and preparing future seafarers through non-traditional learning techniques and programs.

                                The world, after all, is well on its way to automation and digitization. Seaversity aims to contribute to that progression by being the catalyst of tomorrow's maritime fleet.
                            </p>
                        </div>
                    </div>
                     
                </div>
             
            </div>
        </section>
        <!-- End Header -->

        <section class="between team">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <div class="title-main">
                            <h2>Milestone</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                        <div class="news-slider">
                    <div class="mt-30"></div>
                    <div class="owl-carousel owl-theme">

                        <?php $__currentLoopData = $mls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="news-item">
                            <div class="bg-hover"></div>
                            <a class="load-halvor" href="/news/<?php echo e($mls->id); ?>/<?php echo e(Str::slug($mls->title)); ?>">
                                <div class="news-text">
                                    <h3><?php echo e($mls->title); ?></h3>
                                    <img src="<?php echo e(asset('img\arrrow.png')); ?>" alt="">

                                </div>
                                <div class="img-item">
                                <img src="<?php echo e(asset('/uploads/'.$mls->image)); ?>" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </section>


               <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 text-md-left text-center">
                        <!-- head-title -->
                        <div class="head-text">
                            <h2>Corporate Social Responsibility</h2>
                            <div class="mt-10"></div>
                            <p>
Aside from innovating the maritime industry, Seaversity takes part in empowering the forgotten neighbors in partnership with Sorok Uni Foundation to create sustainable community for people who are homeless and affected by Leprosy, help them rebuild their lives and become self-reliant.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                         <div class="img-page-about">
                            <img src="<?php echo e(asset('img\seav\sorok\1.jpg')); ?>" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                         <div class="img-page-about">
                            <img src="<?php echo e(asset('img\seav\sorok\2.jpg')); ?>" class="img-fluid" alt="">
                        </div>
                    </div>
                     <div class="col-md-4 col-sm-12">
                         <div class="img-page-about">
                            <img src="<?php echo e(asset('img\seav\sorok\3.jpg')); ?>" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
               
            </div>
        </section>

        <section class="between team">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        <div class="title-main">
                            <h2>The Leads</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
              
                    <div class="mt-30"></div>
                    
                        <div class="row">
                        <?php $__currentLoopData = $team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4">
                        <div class="team-item">
                            <div class="bg-hover-team"></div>

                            <div class="team-text">
                                
                               
                                <ul>
                                    <li><h3><?php echo e($team->name); ?></h3></li>
                                    <li> <p><?php echo e($team->position); ?></p></li>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>

                            </div>
                            <div class="img-item">
                                 <img src="<?php echo e(asset('/uploads/'.$team->image)); ?>" class="img-fluid" alt="">
                            </div>
                        </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

               </div>
             
            </div>
        </section>

        <!-- Start Footer -->
          <section>
            <div class="container mt-70">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                    <form action="<?php echo e(url('/mailer')); ?>">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="<?php echo e(asset('img\logo.svg')); ?> " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>
        <!-- End Footer -->
    </div>
    <!-- End Content -->
 <script src="<?php echo e(asset('js\plugin.js')); ?>"></script>
    <script src="<?php echo e(asset('js\main.js')); ?>"></script>
       <script src="<?php echo e(asset('js\particles.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin-js\part.js')); ?>"></script>
</body>

</html><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/about.blade.php ENDPATH**/ ?>