<?php $__env->startSection('content'); ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-dice-d6"></i>
                  <?php echo e($data->name); ?>

                </p>
                <a href="/home" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
              <?php if($data->img): ?>
              <div class="row">
                <div class="col-md-12 text-center">
                  <img src="<?php echo e(asset('/uploads/'.$data->img)); ?>" width="40%">
                </div>
              </div>
              <?php else: ?>
              <div></div>
              <?php endif; ?>
          <form action="/admin/product-update/<?php echo e($data->id); ?>" method="POST" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <?php echo method_field('PUT'); ?>
                <div class="row mt-4">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="product name *" name="name" required="" value="<?php echo e($data->name); ?>">
                         <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="service *" name="service" value="<?php echo e($data->service); ?>">
                        <?php $__errorArgs = ['service'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="7" placeholder="description" name="description"><?php echo e($data->description); ?></textarea>
                          <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="unit  *" name="unit" value="<?php echo e($data->unit); ?>">
                        <?php $__errorArgs = ['unit'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="company *" name="company" value="<?php echo e($data->company); ?>">
                        <?php $__errorArgs = ['company'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                        <div class="row">
                    <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        <select class="custom-select"  name="type">
                          <?php if($data->type == 'VR-D'): ?>
                          <option value="<?php echo e($data->type); ?>">VR Deck</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <option value="FFE">Firefighting Equipment</option>
                          <?php endif; ?>
                          <?php if($data->type == 'VR-E'): ?>
                          <option value="<?php echo e($data->type); ?>">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <option value="FFE">Firefighting Equipment</option>
                          <?php endif; ?>
                          <?php if($data->type == 'VR-LFA'): ?>
                          <option value="<?php echo e($data->type); ?>">VR Life Saving Appliances</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="FFE">Firefighting Equipment</option>
                          <?php endif; ?>
                          <?php if($data->type == 'FFE'): ?>
                          <option value="<?php echo e($data->type); ?>">Firefighting Equipment</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <?php endif; ?>
                        </select>
                        <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <select class="custom-select" name="sub_type">
                          <?php if($data->sub_type == 'ME'): ?>
                          <option value="<?php echo e($data->sub_type); ?>">Maritime Education</option>
                          <option value="MT">Maritime Training</option>
                          <option value="CS">Company Specific</option>
                          <?php endif; ?>
                          <?php if($data->sub_type == 'MT'): ?>
                          <option value="<?php echo e($data->sub_type); ?>">Maritime Training</option>
                          <option value="ME">Maritime Education</option>
                          <option value="CS">Company Specific</option>
                          <?php endif; ?>
                          <?php if($data->sub_type == 'CS'): ?>
                          <option value="<?php echo e($data->sub_type); ?>">Company Specific</option>
                          <option value="ME">Maritime Education</option>
                          <option value="MT">Maritime Training</option>
                          <?php endif; ?>
                        </select>
                        <?php $__errorArgs = ['sub_type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="img">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      <?php $__errorArgs = ['img'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <small class="text-danger"><?php echo e($message); ?></small>
                         <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-outline-primary float-right">update</button>
                    </div>
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin_app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\seaversity\resources\views/admin/product.blade.php ENDPATH**/ ?>