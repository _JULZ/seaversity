$(document).ready(function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

if($('.datetimepicker').length > 0 ){
    $('.datetimepicker').datetimepicker({
      format: 'DD/MM/YYYY'
    });
  } 


   $('#table1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  
$('.product_off').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/off_product/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned off succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});

$('.product_on').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/on_product/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned on succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});

    $('.destroy_product').click(function(e){
      e.preventDefault();

        var id = $(this).data('ids');
        var name = $(this).data('names');
   // alert(id);
        Swal.fire({
      title: 'Delete '+name+'?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#7e7e7e',
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        $.ajax({

      url: '/destroy_product/'+id,
      type: 'GET',
      success:function() {
        console.log('success');
    Swal.fire({
      position: 'center',
      type: 'success',
      title: ''+name+' deleted',
      showConfirmButton: false,
      timer: 1500
    })
        setTimeout(function(){
               location.reload();
          }, 1000);
      },

      error:function(){

        }

    });

      }
      })

    });

$('.destroy_partner').click(function(e){
      e.preventDefault();

        var id = $(this).data('ids');
        var name = $(this).data('names');
   // alert(id);
        Swal.fire({
      title: 'Delete '+name+'?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#7e7e7e',
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        $.ajax({

      url: '/destroy_partner/'+id,
      type: 'GET',
      success:function() {
        console.log('success');
    Swal.fire({
      position: 'center',
      type: 'success',
      title: ''+name+' deleted',
      showConfirmButton: false,
      timer: 1500
    })
        setTimeout(function(){
               location.reload();
          }, 1000);
      },

      error:function(){

        }

    });

      }
      })

    });

$('.partner_on').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/on_partner/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned on succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});
});

$('.partner_off').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/off_partner/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned off succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});


$('.destroy_member').click(function(e){
      e.preventDefault();

        var id = $(this).data('ids');
        var name = $(this).data('names');
   // alert(id);
        Swal.fire({
      title: 'Delete '+name+'?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#7e7e7e',
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        $.ajax({

      url: '/destroy_member/'+id,
      type: 'GET',
      success:function() {
        console.log('success');
    Swal.fire({
      position: 'center',
      type: 'success',
      title: ''+name+' deleted',
      showConfirmButton: false,
      timer: 1500
    })
        setTimeout(function(){
               location.reload();
          }, 1000);
      },

      error:function(){

        }

    });

      }
      })

    });

$('.member_off').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/off_member/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned off succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});


$('.member_on').on('click',function(){

var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/on_member/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned on succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});
});

//date picker
$('input[name="date_created"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

// update part - date picker
$('input[name="date_updated"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

//destroy news

$('.destroy_news').click(function(e){
      e.preventDefault();

        var id = $(this).data('ids');
        var name = $(this).data('names');
   // alert(id);
        Swal.fire({
      title: 'Delete '+name+'?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#7e7e7e',
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        $.ajax({

      url: '/destroy_news/'+id,
      type: 'GET',
      success:function() {
        console.log('success');
    Swal.fire({
      position: 'center',
      type: 'success',
      title: ''+name+' deleted',
      showConfirmButton: false,
      timer: 1500
    })
        setTimeout(function(){
               location.reload();
          }, 1000);
      },

      error:function(){

        }

    });

      }
      })

    });

//off status

$('.news_off').on('click',function(){
var id = $(this).data('ids');
var name = $(this).data('names')

//alert(id);
$.ajax({
  url: '/admin/off_news/'+id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned off succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});

//on status

$('.news_on').on('click',function(){
var id = $(this).data('ids');
var name = $(this).data('names')

$.ajax({
  url: '/admin/on_news/' +id,
  type: 'PUT',
  data: {
    'id' : id,
  },

  success:function(){
    console.log('success');

Swal.fire({
  position: 'top',
  type: 'success',
  title: ''+name+' turned on succesfully!',
  showConfirmButton: false,
  timer: 1500
})
    setTimeout(function(){
           location.reload(); 
      }, 1500);
  },

  error:function(){

  }

});

});

});
