<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Team;
use App\Service;
use App\Product;
use App\Partner;

class PagesController extends Controller
{
	//work page
    public function compact_works()
    {
    	$data = Product::where('status', '1')->orderBy('created_at', 'DESC')->get();
        return view('work', compact('data'));
    }
    //home page
    public function compact_home()
    {
    	$data = Product::where('status', '=', '1')->orderBy('created_at', 'DESC')->get();
    	$partner = Partner::where('status', '1')->orderBy('created_at', 'DESC')->get();
    	$news = News::where('type', '=', 'News')->where('status', '=', '1')->orderBy('created_at', 'DESC')->get();
    	return view('home', compact('data','news','partner'));
    }
    //news page
    public function compact_news()
    {
    	$data = News::where('type', 'News')->where('status' , '1')->orderBy('created_at', 'DESC')->get();
    	return view('news', compact('data'));
    }
    //get news id
    public function news_id(News $news, $slug)
    {
    	$data = News::where('id', $news->id)->first();
    	return view('news_id', compact('data'));
    }
    //get product id
    public function product_id(Product $product, $slug)
    {
    	$data = Product::where('id', $product->id)->first();
    	return view('work_id',compact('data'));
    }
    public function product()
    {
        return view('product');
    }
    public function type_deck()
    {   
        $type = "DECK";
        $data = Product::where('type', '=', 'VR-D')->where('status', '1')->orderBy('created_at', 'DESC')->get();
        return view('product_type',compact('data','type'));
    }
     public function type_engine()
    {   
        $type = "ENGINE";
        $data = Product::where('type', '=', 'VR-E')->where('status', '1')->orderBy('created_at', 'DESC')->get();
        return view('product_type',compact('data','type'));
    }
    public function type_lfa()
    {
        $type = "LIFE SAVING APPLIANCES";
        $data = Product::where('type', '=', 'VR-LFA')->where('status', '1')->orderBy('created_at', 'DESC')->get();
        return view('product_type',compact('data','type'));
    }
      public function type_ffe()
    {
        $type = "FIREFIGHTING EQUIPMENT";
        $data = Product::where('type', '=', 'FFE')->where('status', '1')->orderBy('created_at', 'DESC')->get();
        return view('product_type',compact('data','type'));
    }
    public function compact_about()
    {
        $team = Team::where('status', '=', '1')->orderBy('created_at', 'DESC')->get();
        $mls = News::where('type', '=' , 'Milestone')->where('status', '1')->orderBy('created_at', 'ASC')->get();
        return view('about', compact('team','mls'));
    }
    //get partner view
    public function partner_id(Partner $partner, $slug)
    {
        $data = Partner::where('id', $partner->id)->first();
        return view('partner_id', compact('data'));
    }
}
