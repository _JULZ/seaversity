<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

//servie home view
    public function services()
    {
        $data = Service::orderBy('created_at', 'DESC')->simplePaginate(10);
        return view('admin/services', compact('data'));
    }
//add service
    public function add_service(Service $service)
    {
    	$data = request()->validate([
    		'name' => 'required',
    		'type' => 'required',
    		'description' => 'required',
    		'status' => 'required',
    	]);
    	$service->create($data);
    	return back();
    }
//get service id
    public function service_id(Service $service)
    {
    	$data = Service::where('id', '=', $service->id)->first();
    	return view('admin/service_id', compact('data'));
    }
//update service
    public function update_service(Service $service)
    {
    	$data = Service::where('id', '=', $service->id)->first();
    	$data->name = request('name');
    	$data->type = request('type');
    	$data->description = request('description');
    	$data->save();
    	return back();
    }
}
