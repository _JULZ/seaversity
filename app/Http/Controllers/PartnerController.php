<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;

class PartnerController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

//partners view
    public function partners()
    {
        $data = Partner::orderBy('created_at', 'DESC')->paginate(6);
        return view('admin/partners', compact('data'));
    }
//add partner 
    public function add_partner()
    { 
    	$this->validate(request(),
    		[
    			'name' => 'required',
                "filename" => "mimes:jpg,jpeg,png,gif",
    		]);
      $file = request()->file('image');
      $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
      $file->move(base_path().'/public/uploads',$filename);

    	Partner::create([
    		'name' => request('name'),
    		'about' => request('about'),
    		'link' => request('link'),
    		'image' => $filename,
    		'status' => 1,
    	]);
    	return back();
    }
 //delete partner 
    public function destroy_partner(Partner $partner)
    {
    	$data = Partner::where('id', '=', $partner->id)->first();
    	$data->delete();
  		return response()->json();
    }
  //set partner status off
    public function off_status(Partner $partner)
    {
    	$data = Partner::where('id', '=', $partner->id)->first();
    	$data->status = 0;
    	$data->save();
    	return response()->json();
    }
   //set partner status on
    public function on_status(Partner $partner)
     {
    	$data = Partner::where('id', '=', $partner->id)->first();
    	$data->status = 1;
    	$data->save();
    	return response()->json();
    }
    //get partner id
    public function partner_id(Partner $partner)
    {
    	$data = Partner::where('id', '=', $partner->id)->first();
    	return view('admin/partner_id', compact('data'));
    }
    //partner update
    public function update_partner(Partner $partner)
    {
    	$this->validate(request(), 
    	[
    		'name' => 'required',
    		 "filename" => "mimes:jpg,jpeg,png,gif",
    	]);

    if($file = request()->file('img')){
    $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
    $file->move(base_path().'/public/uploads',$filename);
    }else{
    $filename = $partner->image;
    }
    	$data = Partner::where('id', '=', $partner->id)->first();
    	$data->name = request('name');
    	$data->link = request('link');
    	$data->about = request('about');
    	$data->image = $filename;
    	$data->save();
    	return back();
    }
}