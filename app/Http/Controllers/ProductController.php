<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class ProductController extends Controller
{
    public function __construct()
   {
   	$this->middleware('auth');
   }

   //add product 
  public function add_product()
    {

        $this->validate(request(),
           		 [
                'name' => 'required|min:2',
                "filename" => "mimes:jpg,jpeg,png,gif",
                 ]);
      $file = request()->file('img');
      $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
      $file->move(base_path().'/public/uploads',$filename);
           
        Product::create([
            'name' => request('name'),
            'service' => request('service'),
            'description' => request('description'),
            'unit' => request('unit'),
            'company' => request('company'),
            'type' => request('type'),
            'sub_type' => request('sub_type'),
            'img' => $filename,
            'status' => 1,
        ]);
        return back();
    }
      //delete product
    public function destroy_product(Product $product)
    {
        $data = Product::where('id', '=', $product->id)->first();
        $data->delete();
        return response()->json();
    }
    //product off
    public function off_status(Product $product)
    {
    	$data = Product::where('id', '=', $product->id)->first();
    	$data->status = 0;
    	$data->save();
    	return response()->json();
    }
    //product on
    public function on_status(Product $product)
    {
    	$data = Product::where('id', '=', $product->id)->first();
    	$data->status = 1;
    	$data->save();
    	return response()->json();
    }
    //get product id
    public function product_id(Product $product)
    {
    	$data = Product::where('id', '=', $product->id)->first();
    	return view('admin/product',compact('data'));
    }
    //update product
    public function update_product(Product $product)
    {
    	  $this->validate(request(),
           		 [
                'name' => 'required|min:2',
                "filename" => "mimes:jpg,jpeg,png,gif",
                 ]);

   if($file = request()->file('img')){
    $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
    $file->move(base_path().'/public/uploads',$filename);
    }else{
    $filename = $product->img;
    }
        $data = Product::where('id', '=', $product->id)->first();
        $data->name = request('name');
        $data->service = request('service');
        $data->description = request('description');
        $data->unit = request('unit');
        $data->company = request('company');
        $data->type = request('type');
        $data->sub_type = request('sub_type');
        $data->img = $filename;
        $data->save();
        return back();
    }
}
