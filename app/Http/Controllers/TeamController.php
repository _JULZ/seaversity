<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    //team view
    public function team()
    {
        $data = Team::orderBy('created_at', 'DESC')->simplePaginate(4);
        return view('admin/team', compact('data'));
    }
    //add team member
    public function add_member()
    {
    	$this->validate(request(),
    		[
    			'name' => 'required',
    			'position' => 'required',
                "filename" => "mimes:jpg,jpeg,png,gif",
    		]);

      $file = request()->file('image');
      $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
      $file->move(base_path().'/public/uploads',$filename);

    	Team::create([
    		'name' => request('name'),
    		'position' => request('position'),
    		'email' => request('email'),
    		'contact' => request('contact'),
    		'address' => request('address'),
    		'image' => $filename,
    		'status' => 1,
    	]);
    	return back();
    }
    //delete member
    public function destroy_member(Team $team)
    {
    	$data = Team::where('id', '=',  $team->id)->first();
    	$data->delete();
    	return response()->json();
    }
    //status off
    public function off_status(Team $team)
    {
    	$data = Team::where('id', '=', $team->id)->first();
    	$data->status = 0;
    	$data->save();
    	return response()->json();
    }
    //status on
     public function on_status(Team $team)
     {
     	$data = Team::where('id', '=', $team->id)->first();
     	$data->status = 1;
     	$data->save();
     	return response()->json();
     }
     //get member id
     public function member_id(Team $team)
     {
     	$data = Team::where('id', '=' , $team->id)->first();
     	return view('admin/team_id',compact('data'));
     }
     //update memeber
     public function update_member(Team $team)
     {
     	$this->validate(request(),
     		[
     			'name' => 'required',
     			 "filename" => "mimes:jpg,jpeg,png,gif",
     		]);

    if($file = request()->file('image')){
    $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
    $file->move(base_path().'/public/uploads',$filename);
    }else{
    $filename = $team->image;
    }
     	$data = Team::where('id', '=', $team->id)->first();
     	$data->name = request('name');
     	$data->position = request('position');
     	$data->email = request('email');
     	$data->contact = request('contact');
     	$data->address = request('address');
     	$data->image = $filename;
     	$data->save();
     	return back();
     }
}
