<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;


class NewsController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    //News View
    public function news()
    { 
        $data = News::orderBy('created_at', 'DESC')->simplePaginate(5);
        return view('admin/news', compact('data'));
    }
    //add news
    public function add_news() 
    {
    	$this->validate(request(),
        [
          'title' => 'required',
          "filename" => "mimes:jpg,jpeg,png,gif",
        ]);

      $file = request()->file('image');
      $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
      $file->move(base_path().'/public/uploads',$filename);

      News::create([
      	'title' => request('title'),
      	'date_created' => request('date_created'),
      	'content' => request('content'),
      	'type' => request('type'),
      	'image'=> $filename,
      	'status' => 1,
      ]);
      	return back();
    }
    //delete news
    public function destroy_news(News $news)
    {
      $data = News::where('id', '=', $news->id)->first();
      $data->delete();
      return response()->json();
    }
    //set status off
    public function off_status(News $news)
    {
      $data = News::where('id', $news->id)->first();
      $data->status = 0;
      $data->save();
      return response()->json();
    }
    //set status on
    public function on_status(News $news)
    {
      $data = News::where('id', $news->id)->first();
      $data->status = 1;
      $data->save();
      return response()->json();
    }
    //get news id
    public function news_id(News $news)
    {
      $data = News::where('id',  $news->id)->first();
      return view('admin/news_id', compact('data'));
    }
    //update news
    public function update_news(News $news)
    {

      $this->validate(request(), 
      [
         'title' => 'required',
         "filename" => "mimes:jpg,jpeg,png,gif",
      ]);

    if($file = request()->file('image')){
    $filename = md5($file->getClientOriginalName().time()) . "." . $file->getClientOriginalExtension();
    $file->move(base_path().'/public/uploads',$filename);
    }else{
    $filename = $news->image;
    }

      $data = News::where('id', $news->id)->first();
      $data->title = request('title');
      $data->date_created = request('date_updated');
      $data->content = request('content');
      $data->image = $filename;
      $data->save();
      return back();
    }
}