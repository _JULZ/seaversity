<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
	//send mail
    public function send_mail()
    {
    $f_name = request('name');
    $f_email = request('email');
    $f_message = request('message');
    $data = [
        "name" =>  $f_name,
        "email" => $f_email,
        "body" => $f_message,
         ];
        Mail::send('mail', $data, function($message){
            $message->to('lrvl.smtp@gmail.com')->subject('Contact Us '.date('F, d o'));
	        $message->from('lrvl.smtp@gmail.com');
        });
        return back();
    }
    public function mail()
    {
    return view('mail');
    }
}