<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | LMS</title>
    <link rel="stylesheet" href="{{ asset('css\plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('css\style.css') }}">
    <link rel="stylesheet" href="{{ asset('css\responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts\font-awesome\css\all.css') }}">
</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="{{ asset('img\seavclear.gif') }} " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="index.html">
                        <img src="{{ asset('img\logo.svg') }}" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                 <ul class="links">
                   <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link active"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>

                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-detail-work luxy-el" data-horizontal="1" data-speed-y="10">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- head-title -->
                        <div class="head-text">
                            <img src="{{ asset('/img/seav/6.png')}}" width="10%">
                            <h2>LEARNING MANAGEMENT SYSTEM</h2>                          
                        </div>
                           <div class="work-detail-text text-center">
                                <p class="text-light">The Seaversity LMS is a comprehensive system to provide competency management solutions for the maritime industry. Our systems are primarily authored with the top development platforms and e-learning tools to ensure fully interactive and gamified technology. The content is thoroughly developed by Maritime SMEs all over the Philippines so that each training may be cascaded, monitored, and validated anywhere in the world.</p>
    
                            </div>
                    </div>
                </div>
              
                <!-- main background-->
                <div class="work-detail bg-work-1"></div>
                <div id="headmove" class="lines">
                        <div data-depth="0.2" class="img-line">
                            <img src="{{ asset('img\lines.svg') }}" alt="">
                        </div>
                    </div>
            </div>
        </section>
        <!-- End Header -->

        <!-- Start Content -->
        <section class="work-page between">
                <div class="mouse-down page"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 text-md-left text-center">
                        <div class="work-desc-text">
                            <h1 class="text-light">Learning Management System</h1>
                            <p>The e-learning system is able to connect the gap between training and learning which seems to be one of the problems of the maritime industry.</p>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="img-work-detail">
                              <div class="img-work-detail">
                            <div class="card">
                                <div class="card-header bg-primary text-center">
                                   <h4> <strong class="text-light">BENEFITS</strong></h4>
                                </div>
                            <div class="card-body">
                                <h5 class="card-title"><strong>CUT DOWN EXPENSES</strong></h5>
                                    <p class="card-text text-dark">Reduce the use of paper for record tracking or lesson print-outs.</p>
                                <h5 class="card-title"><strong>MAXIMIZE HUMAN RESOURCE</strong></h5>
                                     <p class="card-text text-dark">Utilize workforce to focus on the company goal whether it be giving lectures or monitoring progress.</p>
                                <h5 class="card-title"><strong>STREAMLINE FILE TRACKING</strong></h5>
                                     <p class="card-text text-dark">Using the platform content management system, users can easilyaccess files amd needed.</p>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>



                    <div class="col-md-8">
                        <div class="img-work-detail">
                            <div class="card">
                                <div class="card-header bg-primary text-center">
                                   <h4> <strong class="text-light">FEATURES</strong></h4>
                                </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                <h5 class="card-title"><strong>INTERACTIVE MATERIALS</strong></h5>
                                    <p class="card-text text-dark">Interactive platforms provide hands-on learning that increases information retention and critical thinking.</p>
                                     <br>
                                 </div>
                                 <div class="col-md-6 col-sm-12">
                                      <h5 class="card-title"><strong>PROGRESS TRACKING</strong></h5>
                                     <p class="card-text text-dark">Multiple rules not limited to “student” and “teacher capabilities allow specific and focused training strategies while monitoring learning efficiency.</p>
                                     <br>
                                    </div>
                                 </div>

                                  <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                <h5 class="card-title"><strong>24/7 ACCESS</strong></h5>
                                    <p class="card-text text-dark">Its online and offline features allow content access anytime, anywhere.</p>
                                    <br>
                                 </div>
                                 <div class="col-md-6 col-sm-12">
                                      <h5 class="card-title"><strong>EVALUATION SYSTEM</strong></h5>
                                     <p class="card-text text-dark">Grading and individual evaluation can be used to instantly record and track performance-based assessment.</p>
                                     <br>
                                 </div>
                                 </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<br>
<br>
<br>
<br>
            <div class="container mt-5">
                <div class="row text-md-right">
                    <div class="col-md-4 col-sm-12">
                      <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="https://lms.seaversity.com.ph/" target="_blank" class="button_su_inner">
                                    <span class="button_text_container">
                                        EXPLORE OUR LMS &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                            <br>
                            <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="http://ecat.seaversity.com.ph/democourse/" class="button_su_inner" target="_blank">
                                    <span class="button_text_container">
                                       TRY THE DEMO COURSE &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                            <br>
                            <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="http://ecat.seaversity.com.ph/demoax/" class="button_su_inner" target="_blank">
                                    <span class="button_text_container">
                                        TRY THE DEMO ASSESTMENT &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>

                      </div>
                      
                       <div class="col-md-8 col-sm-12">
                      <iframe width="730" height="360" src="https://www.youtube.com/embed/pQqJLH_tQI8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>

            </div>
        </div>
    <br>
    <br>

     <div class="container mt-100">
                <div class="row text-center">
                    <div class="col-md-6 col-sm-12">
                    <!--     <br>
                        <br>
                        <br>
                        <br> -->
                        <div class="head-text mt-90">
                            <h2>SEAVERSITY AX</h2>                          
                        </div>
                          <p>Grading and individual evaluation can be used to instantly record and track performance-based assessment.</p>
                          <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="/ax" class="button_su_inner">
                                    <span class="button_text_container">
                                        TRY THE DEMO ASSESSMENT &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                      </div>
                      
                       <div class="col-md-6 col-sm-12">
                      <img src="{{asset('/img/seav/laptop.png')}}">
                      </div>

            </div>
        </div>

 <div class="container mt-100">
                <div class="row text-center">
                     <div class="col-md-6 col-sm-12">
                      <img src="{{asset('/img/seav/DEVICE-03.png')}}" width="80%">
                      </div>
                      
                       <div class="col-md-6 col-sm-12">
                        <div class="head-text mt-90">
                            <h2>ELECTRONIC TRAINING RECORD BOOK</h2>                          
                        </div>
                          <p>The ETRB addresses the problem of the TRB by making tracking digital. It features built-in real-time task monitoring, learning materials, assessments and evaluations, all for an efficient and modernized tracking of a user’s progress on board.</p>
                          <div class="button_su mt-10">
                                <span class="su_button_circle">
                                </span>
                                <a href="https://lms.seaversity.com.ph/" target="_blank" class="button_su_inner">
                                    <span class="button_text_container">
                                        VISIT OUR ETRB &nbsp;<i class="fa fa-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                      </div>

            </div>
        </div>
        </section>
        <!-- End Content -->


        <!-- Start Footer -->
   <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="{{url('/mailer')}}">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="{{ asset('img\logo.svg') }} " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>  
        <!-- End Footer -->
    </div>
    <!-- End Content -->

    <script src="{{ asset('js\plugin.js') }}"></script>
    <script src="{{ asset('js\main.js') }}"></script>
</body>

</html>
