<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | Portfolio</title>

    <link rel="stylesheet" href="{{ asset('css\plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('css\style.css') }}">
    <link rel="stylesheet" href="{{ asset('css\responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts\font-awesome\css\all.css') }}">
</head>
<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="{{ asset('img\seavclear.gif') }} " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="/">
                        <img src="{{ asset('img\logo.svg') }}" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                <ul class="links">
                   <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link active"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>


                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-page">
             <div class="lines-home-2">
                <div data-depth="0.2" class="img-line">
                   <div id="particles-js"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12 text-md-left text-center mt-30">

                        <!-- head-title -->
                        <div class="head-text">
                            <h2>OUR PORTFOLIO</h2>
                            <div class="mt-10"></div>
                            
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End Header -->

        <section class="main-work between">
            <div class="container">
                <div class="row">
                    <!-- work item -->
                    @foreach($data as $data)
                    <div class="col-md-3">
                        <div class="work-item">
                            <a class="load-halvor" href="/works/{{$data->id}}/{{Str::slug($data->name)}}">
                                <div class="work-text">
                                    <h3 class="text-light">{{$data->name}}</h3>
                                    <p class="text-light">{{$data->type}}</p>
                                    <span>EXPLORE</span>
                                </div>
                                <div class="border-work"></div>
                                <div class="img-item">
                                <img src="{{ asset('/uploads/'.$data->img)}}" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                

                </div>
            </div>
        </section>



          <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <div class="">
                            <h3 class="text-light text-left">CONTACT US !</h3>
                             <i class="fas fa-envelope text-primary" aria-hidden="true">&nbsp;&nbsp;info@seaversity.com.ph</i>
                            <br>
                             <i class="fas fa-phone text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;0936-936-2729</i>
                             <br>
                            <i class="fas fa-location-arrow text-primary mt-2" aria-hidden="true">&nbsp;&nbsp;ECJ Building, Real St. Cor. Arzobispo St.,Intramuros, Manila
                            </i>
                        </div>
                        <br>
                    </div>

                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-left">TALK TO US !</h3>
                   <form action="{{url('/mailer')}}">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="your name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="your email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="your message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="col-md-4  text-center">
                       <div class="">
                         <h3 class="text-light">SOCIAL MEDIA</h3>
                             <a href="https://www.facebook.com/SeaversityPH/"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;SeaversityPH</a>
                             <br>
                                <a href="https://twitter.com/SeaversityPH"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp;@seaversity_innovations</a>
                                <br>
                                <a href="https://www.instagram.com/seaversityPH/"><i class="fab fa-instagram" aria-hidden="true"></i>&nbsp;@seaversity</a>
                                <br>
                                <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ"><i class="fab fa-youtube" aria-hidden="true"></i>&nbsp;Seaversity Innovations</a>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="{{ asset('img\logo.svg') }} " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
                       
            </div>
 </section>
    </div>
    <!-- End Content -->
<script src="{{ asset('js\plugin.js') }}"></script>
    <script src="{{ asset('js\main.js')}}"></script>
      <script src="{{ asset('js\particles.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin-js\part.js') }}"></script>
</body>

</html>