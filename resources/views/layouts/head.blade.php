 	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity</title>
    <link rel="stylesheet" href="{{ asset('css\plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('css\style.css') }}">
    <link rel="stylesheet" href="{{ asset('css\responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts\font-awesome\css\all.css') }}">