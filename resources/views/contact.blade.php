<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Seaversity | Contact us</title>
    <link rel="stylesheet" href="{{ asset('css\plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('css\style.css') }}">
    <link rel="stylesheet" href="{{ asset('css\responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts\font-awesome\css\all.css') }}">
</head>

<body>

    <!-- Start Preload -->
    <div class="preloader">
    </div>
    <div class="block-1"></div>
    <div class="block-2"></div>
    <div class="logo-load">
        <img src="{{ asset('img\seavclear.gif') }} " alt="">
    </div>
    <div class="logo-load spinning"></div>
    <div class="over-all"></div>
    <!-- End Preload -->

    <!-- Start Navbar -->
    <nav>
        <div class="container">
            <div class="navbars">

                <!-- logo -->
                <div class="logo">
                    <a class="load-halvor" href="index.html">
                        <img src="{{ asset('img\logo.svg') }}" alt="">
                    </a>

                </div>

                <!-- navbar link -->
                <ul class="links">
                          <li class="normal-link"><a class="load-halvor" data-link='HOME' href="/">HOME</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='PRODUCTS' href="/products">PRODUCTS</a>
                    </li>
                     <li class="normal-link"><a class="load-halvor" data-link='SERVICES' href="/services">SERVICES</a>
                    </li>
                    <li class="normal-link"><a class="load-halvor" data-link='PORTFOLIO' href="/portfolio">PORTFOLIO</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='ABOUT' href="/about">ABOUT</a></li>
                    <li class="normal-link"><a class="load-halvor" data-link='NEWS' href="/news">NEWS</a></li>
                    <li>
                        <div class="button_su">
                            <span class="su_button_circle">
                            </span>
                            <a href="/contact" class="button_su_inner load-halvor">
                                <span class="button_text_container">
                                    CONTACT US
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

                <!-- mobile menu -->
                <div class="toggle">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>


            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Start Content -->
    <div id="halvor">

        <!-- Start Header -->
        <section class="header-page">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12 text-md-left text-center mt-30">
                    </div>
                </div>
            </div>
        </section>
        <!-- End Header -->

        <section class="contact between text-center">
             <div class="lines-home-2">
                <div data-depth="0.2" class="img-line">
                   <div id="particles-js"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">

                <div class="col-md-4">
                </div>
                <div class="col-md-4 text-center">
                 <div class="">
                  <h3 class="text-light text-center">CONTACT US !</h3>
                    <form action="{{url('/mailer')}}">
                    <div class="form-group">
                     <input type="text" class="form-control" name="name"  placeholder="name *">
                      </div>
                       <div class="form-group">
                        <input type="email" class="form-control" name="email"  placeholder="email *">
                       </div>
                      <div class="form-group">
                     <textarea class="form-control" placeholder="message *" name="message"  rows="7"></textarea>
                    </div>
                   <button class="btn btn-outline-primary btn-block">send</button> 
                  </form>
                   <br>
                    </div>
                     </div>
                      <div class="md-4">
                             <div class="col-md-1 offset-md-5 text-right mt-70">
                        <!-- social media header -->
                        <div class="sosmed">
                            <ul>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.facebook.com/SeaversityPH/" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://twitter.com/SeaversityPH" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.instagram.com/seaversityPH/" target="_blank" class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-instagram" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="button_su_social">
                                        <span class="su_button_circle_social">
                                        </span>
                                        <a href="https://www.youtube.com/channel/UCFcNRG_C3mXI7vr2VOvVfPQ" target="_blank"  class="button_su_inner_social">
                                            <span class="button_text_container_social">
                                                <i class="fab fa-youtube" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                      </div>

                </div>
            </div>
        </section>


        <!-- Start Footer -->
        <section>
            <div class="container">
                <div class="row">
                     <div class="col-md-12 text-center">
                         <div class="copy">
                        <img src="{{ asset('img\logo.svg') }} " alt="">
                            <p>Copyright @ Seaversity | All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
       </section>
        <!-- End Footer -->
    </div>
    <!-- End Content -->

    <script src="{{ asset('js\plugin.js') }}"></script>
    <script src="{{ asset('js\main.js') }}"></script>
      <script src="{{ asset('js\particles.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin-js\part.js') }}"></script>
</body>

</html>