@extends('admin/layout/admin_app')

@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="row">
            <div class="col-md-12">
             <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-user-friends"></i>
                  Team
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-member">
                <i class="fas fa-plus">&nbsp;</i>Add Team Member
                </button>
              </div>
              </div>
            </div>
          </div>
          <div class="card-body">
           <table class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Contact</th>
                  <th>Address</th>
                  <th>Photo</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                <tr>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->position}}</td>
                  <td>{{$dt->contact}}</td>
                  <td>{{$dt->address}}</td>
                  @if(!($dt->image))
                  <td></td>
                  @else
                  <td><img src="{{ asset('/uploads/'.$dt->image)}}" width="100 px"></td> 
                  @endif
                  @if($dt->position == 'COO & President' || $dt->position == 'Co-Founder')
                  <td class="text-center">
                       @if($dt->status == '1')
                    <button class="btn btn-secondary btn-sm mt-2 member_off" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">OFF</button>
                    @else
                    <button class="btn btn-primary btn-sm mt-2 member_on" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">ON</button>
                    @endif
                    <br>
                    <a href="{{url('admin/member/').'/'.$dt->id}}" class="btn btn-primary btn-sm mt-2"><i class="fa fa-pen"></i></a> 
                  </td>
                  @else
                   <td class="text-center">
                      @if($dt->status == '1')
                    <button class="btn btn-secondary btn-sm mt-2 member_off" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">OFF</button>
                    @else
                    <button class="btn btn-primary btn-sm mt-2 member_on" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">ON</button>
                    @endif
                    <br>
                    <a href="{{url('admin/member/').'/'.$dt->id}}" class="btn btn-primary btn-sm mt-2"><i class="fa fa-pen"></i></a>
                    <button class="btn btn-danger btn-sm destroy_member mt-2"   type="button" data-ids="{{$dt->id}}" data-names="{{$dt->name}}"><i class="fas fa-trash"></i></button>  
                  </td>
                  @endif
                </tr>
             @endforeach
                </tfoot>
            </table>
            <div class="mt-3 float-right">  {{ $data->links() }}</div>
        </div>
        </div>
      <!--modal-->
      <div class="modal fade" id="add-member">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-user-friends">&nbsp;</i>Add Team Member</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
          <div class="modal-body bg-white">
              <form action="/admin/add_member" method="POST" enctype="multipart/form-data">
                @csrf
             <div class="row">
              <div class="col-md-6 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="name *" name="name" required="">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="position *" name="position" required="">
                         @error('position')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
               </div>
              </div>
              <!-- second col-->
              <div class="row">
              <div class="col-md-6 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="email *" name="email">
                         @error('email')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="contact *" name="contact">
                         @error('contact')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
               </div>
              </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <!-- textarea -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="address *" name="address">
                         @error('address')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                      </div>
                    </div>
                      <div class="row">
                    <div class="col-md-12">
                      <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" required="" name="image">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('image')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
                  </div>
                  </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </form>
          </div>
        </div>
          <!-- /.modal-content -->
        </div>
      </div>
      <!-- /.container-fluid -->
    </section> 
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection