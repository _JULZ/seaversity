@extends('admin/layout/admin_app')
@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="row">
            <div class="col-md-12">
             <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-newspaper"></i>
                  News
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-news">
                <i class="fas fa-plus">&nbsp;</i>Add News
                </button>
              </div>
              </div>
            </div>
          </div>
        <div class="card-body">
          <table  class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Content</th>
                  <th>Image</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                <tr>
                  <td>{{$dt->title}}</td>
                  <td>{{$dt->date_created}}</td>
                  <td>{{$dt->content}}</td>
                  @if(!($dt->image))
                  <td></td>
                  @else
                  <td><img src="{{ asset('/uploads/'.$dt->image)}}" width="100 px"></td> 
                  @endif
                   <td class="text-center">
                    @if($dt->status == '1')
                    <button class="btn btn-secondary btn-sm mt-2 news_off" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">OFF</button>
                    @else
                    <button class="btn btn-primary btn-sm mt-2 news_on" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">ON</button>
                  @endif
                  <br>
                    <a href="{{url('admin/news/').'/'.$dt->id}}" class="btn btn-primary btn-sm mt-2"><i class="fa fa-pen"></i></a>
                    <button class="btn btn-danger btn-sm destroy_news mt-2"   type="button" data-ids="{{$dt->id}}" data-names="{{$dt->title}}"><i class="fas fa-trash"></i></button>  
                </td>
                </tr>
             @endforeach
                </tfoot>
             
            </table>
            <div class="mt-3 float-right">  {{ $data->links() }}</div>
      </div>
             
        </div>
         <!--modal-->
      <div class="modal fade" id="add-news">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-newspaper">&nbsp;</i>Add News</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
          <div class="modal-body bg-white">
              <form action="{{ url('/admin/add_news')}}" method="POST" enctype="multipart/form-data">
                @csrf
             <div class="row">
              <div class="col-md-6 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="title *" name="title" required="">
                         @error('title')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <input type="text" class="form-control" name="date_created" required="">
                         @error('date_created')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
               </div>
              </div>
              <!-- second col-->
              <div class="row">
              <div class="col-md-12 col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                         <textarea class="form-control" rows="6" placeholder="content *" name="content" ></textarea>
                         @error('content')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                         <select class="custom-select"  name="type">
                          <option value="News">News</option>
                          <option value="Milestone">Milestone</option>
                        </select>
                         @error('type')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                      <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" required="" name="image">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('image')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
              </div>      
              </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </form>
          </div>
        </div>
          <!-- /.modal-content -->
        </div>
      </div>
      <!-- /.container-fluid -->
    </section> 
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection