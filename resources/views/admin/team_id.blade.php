@extends('admin/layout/admin_app')
@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-users"></i>
                  {{$data->name}}
                </p>
                <a href="{{url('/admin/team')}}" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
               @if($data->image)
              <div class="row">
                <div class="col-md-12 text-center">
                  <label class="text-center">Current Photo of {{$data->name}}</label>
                  <br>
                  <img src="{{ asset('/uploads/'.$data->image)}}" width="40%">
                </div>
              </div>
              @else
              <div></div>
              @endif
          <form action="/admin/member-update/{{$data->id}}" method="POST" enctype="multipart/form-data">
             @METHOD('PUT')
                  @csrf
                <div class="row mt-4">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="name *" name="name" required="" value="{{$data->name}}">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                        <input type="text" class="form-control" placeholder="position *" name="position" value="{{$data->position}}">
                         @error('position')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="email *" name="email" value="{{$data->email}}">
                         @error('email')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                        <input type="text" class="form-control" placeholder="contact *" name="contact" value="{{$data->contact}}">
                         @error('contact')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="address *" name="address" value="{{$data->address}}">
                         @error('address')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                     <div class="col-md-12 ">
                       <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" name="image">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('image')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                      <button class="btn btn-outline-primary btn-sm float-right mt-2">Update</button>
                    </div>
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection