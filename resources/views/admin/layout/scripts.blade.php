<script src="{{asset('admin-js/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin-js/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('admin-js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<!-- jQuery Knob Chart -->
<script src="{{asset('admin-js/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin-js/moment.min.js')}}"></script>
<script src="{{asset('admin-js/flipclock.js')}}"></script>
<script src="{{asset('admin-js/dev.js')}}"></script>

<script src="{{asset('admin-js/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('admin-js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('admin-js/summernote-bs4.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('admin-js/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin-js/dataTables.bootstrap4.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin-js/bootstrap-switch.min.js') }}"></script>

<!-- overlayScrollbars -->
<script src="{{asset('admin-js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{asset('admin-js/sweetalert2.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin-js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin-js/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin-js/demo.js')}}"></script>
<script src="{{asset('admin-js/jquery-confirm.js')}}"></script>