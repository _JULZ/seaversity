
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
  &nbsp;  &nbsp;  &nbsp;   &nbsp;  &nbsp;  <img src="{{asset('img/logo.svg')}}"  class="brand text-center" height="45"
          >
      <span class="brand-text font-weight-light"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('plugin-img/user-rounded.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
               <li class="nav-item">
                          <a href="{{url('/home')}}"  class="nav-link">
                            <i class="nav-icon fas fa-dice-d6"></i>
                            <p>
                                Products
                            </p>
                          </a>
                        </li>


          <li class="nav-item">
            <a href="{{url('admin/services')}}" class="nav-link">
              <i class="nav-icon fas fa-vr-cardboard"></i>
              <p>
                Services
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ url('/admin/partners')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Partners
              </p>
            </a>
            
          </li>
          <li class="nav-item has-treeview">
            <a href="/admin/news" class="nav-link">
              <i class="nav-icon far fa-newspaper"></i>
              <p>
                News Section
              </p>
            </a>
          </li>
          
      <li class="nav-item">
        <a href="{{url('/admin/team')}}" class="nav-link">
             <i class="nav-icon fas fa-user-friends"></i>
                  <p>
                       Team
                   </p>
              </a>
        </li>

            <li class="nav-item">
                              <a href="{{url('/admin/result')}}" class="nav-link">
                                <i class="nav-icon fas fa-poll"></i>
                                <p>
                                AX Result
                                </p>
                              </a>
                            </li>
        </ul>
      </nav>
      <div class="text-center" style="margin-top: 3%;">
                           <button class="btn btn-outline-primary btn-sm ml-0" align="center" href="{{ route('logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">Logout</button>
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                          </form>
                        </div>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
