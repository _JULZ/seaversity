<!DOCTYPE html>
<html lang="en">

<head>

@include('admin/layout/head')

</head>


<body>
 @include('admin/layout/fb')	

  @include('admin/layout/sidebar')
  @include('admin/layout/topbar')

  @section('content')
@show


@include('admin/layout/aside')

</body>

@include('admin/layout/scripts')
@include('admin/layout/footer')


</html>