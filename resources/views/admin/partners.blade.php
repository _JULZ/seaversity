@extends('admin/layout/admin_app')

@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="row">
            <div class="col-md-12">
             <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-users"></i>
                  Partners
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-partner">
                <i class="fas fa-plus">&nbsp;</i>Add Partners
                </button>
              </div>
              </div>
            </div>
          </div>
        <div class="card-body">
           <table class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Partner name</th>
                  <th>About</th>
                  <th>link</th>
                  <th>Image</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                <tr>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->about}}</td>
                  <td>{{$dt->link}}</td>
                  @if(!($dt->image))
                  <td></td>
                  @else
                  <td><img src="{{ asset('/uploads/'.$dt->image)}}" width="100 px"></td> 
                  @endif
                  <td class="text-center">
                     @if($dt->status == '1')
                    <button class="btn btn-secondary btn-sm mt-2 partner_off" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">OFF</button>
                    @else
                    <button class="btn btn-primary btn-sm mt-2 partner_on" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">ON</button>
                    @endif
                    <br>
                    <a href="{{url('admin/partner/').'/'.$dt->id}}" class="btn btn-primary mt-2 btn-sm"><i class="fa fa-pen"></i></a>
                    <button class="btn btn-danger btn-sm mt-2 destroy_partner"   type="button" data-ids="{{$dt->id}}" data-names="{{$dt->name}}"><i class="fas fa-trash"></i></button>  
                  </td>
                </tr>
             @endforeach
                </tfoot>
              </table>
           <div class="mt-3 float-right">  {{ $data->links() }}</div>
        </div>
      </div>
        <!--/card-->

 <div class="modal fade" id="add-partner">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-users">&nbsp;</i>Add Partner</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
           
          <div class="modal-body bg-white">
              <form action="/admin/add_partner" method="POST" enctype="multipart/form-data">
                @csrf
             <div class="row">
              <div class="col-md-12">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="partner name *" name="name" required="">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="8" placeholder="about *" name="about" ></textarea>
                        @error('about')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <!-- textarea -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="link *" name="link">
                         @error('link')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                      </div>
                    </div>
                      <div class="row">
                    <div class="col-md-12">
                      <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile" required="" name="image">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('image')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
                  </div>
                  </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </div>
           </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div> 

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection