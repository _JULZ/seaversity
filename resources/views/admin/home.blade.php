@extends('admin/layout/admin_app')

@section('content')

<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="card">
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-dice-d6"></i>
                  Products
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-product">
                <i class="fas fa-plus">&nbsp;</i>Add Product
                </button> 
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
              <table id="table1" class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Product name</th>
                  <th>Service</th>
                  <th>Description</th>
                  <th>Unit</th>
                  <th>Companies</th>
                  <th>Photo</th>
                  <th>Type</th>
                  <th>Sub type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  @foreach($data as $dt)
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->service}}</td>
                  <td>{{$dt->description}}</td>
                  <td>{{$dt->unit}}</td>
                  <td>{{$dt->company}}</td>
                  @if(!($dt->img))
                  <td></td>
                  @else
                  <td><img src="{{ asset('/uploads/' .$dt->img)}}" width="100 px"></td> 
                  @endif
                  <td>{{$dt->type}}</td>
                  <td>{{$dt->sub_type}}</td>
                  <td class="text-center">
                    @if($dt->status == '1')
                    <button class="btn btn-secondary btn-sm mt-2 product_off" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">OFF</button>
                    @else
                    <button class="btn btn-primary btn-sm mt-2 product_on" data-ids="{{$dt->id}}" data-names="{{$dt->name}}">ON</button>
                    @endif
                    <br>
                   <a href="{{url('admin/product/').'/'.$dt->id}}" class="btn btn-primary mt-2 btn-sm"><i class="fa fa-pen"></i></a>
                  <button class="btn btn-danger btn-sm destroy_product mt-2"   type="button" data-ids="{{$dt->id}}" data-names="{{$dt->name}}"><i class="fas fa-trash"></i></button>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
            <div class="mt-3 float-right">  {{ $data->links() }}</div>
              
             </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--modal-->
  <div class="modal fade" id="add-product">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-primary">
            <div class="modal-header">
              <p class="modal-title"><i class="fas fa-plus">&nbsp;</i>Add Product</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body bg-white">
              <form action="/admin/add-product" method="POST" enctype="multipart/form-data">
                @csrf
             <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="product name *" name="name" required="">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="service *" name="service" >
                        @error('service')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <!-- textarea -->
                      <div class="form-group">
                        <textarea class="form-control" rows="8" placeholder="description *" name="description" ></textarea>
                        @error('description')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="unit  *" name="unit" >
                        @error('unit')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="company *" name="company">
                        @error('company')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                        <div class="row">
                    <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        <select class="custom-select"  name="type">
                          <option value="VR-D">VR Deck</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <option value="FFE">Firefighting Equipment</option>
                        </select>
                        @error('type')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <select class="custom-select" name="sub_type">
                          <option value="ME">Maritime Education</option>
                          <option value="MT">Maritime Training</option>
                          <option value="CS">Company Specific</option>
                        </select>
                        @error('sub_type')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" required="" name="img">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('img')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
                  </div>
            </div>
            <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>  
</div>
</div>
</section>
<!-- ./wrapper --> 
@endsection
