@extends('admin/layout/admin_app')

@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="card">
        <div class="row">
          <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-vr-cardboard"></i>
                  Services
                </p>
                 <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-service">
                <i class="fas fa-plus">&nbsp;</i>Add Services
                </button>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
               <table class="table table-bordered table-responsive-md table-hover">
                <thead>
                <tr>
                  <th>Service name</th>
                  <th>Description</th>
                  <th>Type</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                <tr>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->description}}</td>
                  <td>{{$dt->type}}</td>
                  <td class="text-center">
                    <a href="{{url('admin/service/').'/'.$dt->id}}" class="btn btn-primary btn-sm"><i class="fa fa-pen"></i></a>
                  </td>
                </tr>
             @endforeach
                </tfoot>
              </table>
              <div class="mt-3 float-right">  {{ $data->links() }}</div>
             </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="add-service">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-primary">
               <p class="modal-title"><i class="fas fa-vr-cardboard">&nbsp;</i>Add Service</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
           
          <div class="modal-body bg-white">
              <form action="/admin/add_service" method="POST">
                @csrf
             <div class="row">
              <div class="col-md-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="service name *" name="name" required="">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <select class="custom-select"  name="type">
                          <option>select type</option>
                          <option value="VR">VR</option>
                          <option value="e-Learning">e-Learning</option>
                        </select>
                        @error('service')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <!-- textarea -->
                      <div class="form-group">
                        <textarea class="form-control" rows="8" placeholder="description *" name="description"></textarea>
                        @error('description')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                        <input type="hidden" name="status" value="1">
                      </div>
                    </div>
                  </div>
            </div>
             <div class="modal-footer bg-primary">
            <button type="submit" class="btn btn-outline-light btn-sm float-right">submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div> 
</section>
</div>
</div>
<!-- ./wrapper --> 
@endsection
