@extends('admin/layout/admin_app')

@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
           
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-vr-cardboard"></i>
                  {{$data->name}}
                </p>
                <a href="{{url('/admin/services')}}" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
          <form action="/admin/service-update/{{$data->id}}" method="POST">
             @METHOD('PUT')
                  @csrf
                <div class="row">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="service name *" name="name" required="" value="{{$data->name}}">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                        <select class="custom-select"  name="type">
                        @if($data->type == 'e-Learning')
                        <option value="{{$data->type}}">e-Learning</option>
                        <option value="VR">VR</option>
                        @endif
                        @if($data->type == 'VR')
                        <option value="{{$data->type}}">VR</option>
                        <option value="e-Learning">e-Learning</option>
                        @endif
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="service description" name="description">{{$data->description}}</textarea>
                         @error('description')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                     <div class="col-md-12 ">
                      <button class="btn btn-outline-primary btn-sm float-right">Update</button>
                    </div>
                  </div>
                   
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection