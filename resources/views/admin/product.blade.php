@extends('admin/layout/admin_app')

@section('content')
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <div class="card">
                 <div class="row">
                  <div class="col-md-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <p class="card-title text-center">
                  <i class="fas fa-dice-d6"></i>
                  {{$data->name}}
                </p>
                <a href="/home" class="btn btn-primary btn-sm float-right"> <i class="fas fa-angle-double-left">&nbsp;</i>Back</a>
              </div>
                 </div>
               </div>
             </div>
             <div class="card-body">
              @if($data->img)
              <div class="row">
                <div class="col-md-12 text-center">
                  <img src="{{ asset('/uploads/'.$data->img)}}" width="40%">
                </div>
              </div>
              @else
              <div></div>
              @endif
          <form action="/admin/product-update/{{$data->id}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @METHOD('PUT')
                <div class="row mt-4">
                  <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="product name *" name="name" required="" value="{{$data->name}}">
                         @error('name')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="service *" name="service" value="{{$data->service}}">
                        @error('service')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" rows="7" placeholder="description" name="description">{{$data->description}}</textarea>
                          @error('description')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="unit  *" name="unit" value="{{$data->unit}}">
                        @error('unit')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="company *" name="company" value="{{$data->company}}">
                        @error('company')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                        <div class="row">
                    <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        <select class="custom-select"  name="type">
                          @if($data->type == 'VR-D')
                          <option value="{{$data->type}}">VR Deck</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <option value="FFE">Firefighting Equipment</option>
                          @endif
                          @if($data->type == 'VR-E')
                          <option value="{{$data->type}}">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          <option value="FFE">Firefighting Equipment</option>
                          @endif
                          @if($data->type == 'VR-LFA')
                          <option value="{{$data->type}}">VR Life Saving Appliances</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="FFE">Firefighting Equipment</option>
                          @endif
                          @if($data->type == 'FFE')
                          <option value="{{$data->type}}">Firefighting Equipment</option>
                          <option value="VR-E">VR Engine</option>
                          <option value="VR-D">VR Deck</option>
                          <option value="VR-LFA">VR Life Saving Appliances</option>
                          @endif
                        </select>
                        @error('type')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <select class="custom-select" name="sub_type">
                          @if($data->sub_type == 'ME')
                          <option value="{{$data->sub_type}}">Maritime Education</option>
                          <option value="MT">Maritime Training</option>
                          <option value="CS">Company Specific</option>
                          @endif
                          @if($data->sub_type == 'MT')
                          <option value="{{$data->sub_type}}">Maritime Training</option>
                          <option value="ME">Maritime Education</option>
                          <option value="CS">Company Specific</option>
                          @endif
                          @if($data->sub_type == 'CS')
                          <option value="{{$data->sub_type}}">Company Specific</option>
                          <option value="ME">Maritime Education</option>
                          <option value="MT">Maritime Training</option>
                          @endif
                        </select>
                        @error('sub_type')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="img">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      @error('img')
                            <small class="text-danger">{{$message}}</small>
                         @enderror
                    </div>
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-outline-primary float-right">update</button>
                    </div>
                  </form>
                  </div>
             </div>
           </div>
             </div><!-- /.container-fluid -->
           </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
@endsection