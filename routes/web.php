<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//home page 
Route::get('/', 'PagesController@compact_home');

//services page
Route::get('/services', function () {
    return view('services');
});
//lms page
Route::get('/lms', function () {
    return view('lms');
});
//ax page
Route::get('/ax', function () {
    return view('ax');
});
//about page
Route::get('/about', 'PagesController@compact_about');
//product page
Route::get('/products', 'PagesController@product');
//product type deck
Route::get('/deck', 'PagesController@type_deck');
//engine
Route::get('/engine', 'PagesController@type_engine');
//lfa 
Route::get('/vr-lfa', 'PagesController@type_lfa');
//ffe
Route::get('/vr-ffe', 'PagesController@type_ffe');
//news page
Route::get('/news', 'PagesController@compact_news');
//get news id
Route::get('/news/{news}/{slug}', 'PagesController@news_id');
//compact works
Route::get('/portfolio', 'PagesController@compact_works');
//get work id
Route::get('/works/{product}/{slug}', 'PagesController@product_id');
//get partner id
Route::get('/partner/{partner}/{slug}', 'PagesController@partner_id');

//contact page
Route::get('/contact', function () {
    return view('contact');
});
//talk to us send mail
Route::get('/mailer', 'MailController@send_mail');
//mail view
Route::get('/mail', 'MailController@mail');


Auth::routes();

//Work view
Route::get('/admin/work', 'HomeController@work');
//AX result View
Route::get('/admin/result', 'HomeController@result');

//Admin home view
Route::get('/home', 'HomeController@index');
//add product
Route::post('/admin/add-product', 'ProductController@add_product');
//delete product using ajax
Route::get('/destroy_product/{product}', 'ProductController@destroy_product');
//off product status
Route::put('/admin/off_product/{product}', 'ProductController@off_status');
//on product status
Route::put('/admin/on_product/{product}', 'ProductController@on_status');
//get product view id
Route::get('/admin/product/{product}', 'ProductController@product_id');
//update product
Route::put('/admin/product-update/{product}', 'ProductController@update_product');

//Service View 
Route::get('/admin/services', 'ServiceController@services');
//add service
Route::post('/admin/add_service', 'ServiceController@add_service');
//get service  view id
Route::get('/admin/service/{service}', 'ServiceController@service_id');
//update service
Route::put('/admin/service-update/{service}', 'ServiceController@update_service');

//Partner view
Route::get('/admin/partners', 'PartnerController@partners');
//add partner
Route::post('/admin/add_partner', 'PartnerController@add_partner');
//delete partner
Route::get('/destroy_partner/{partner}', 'PartnerController@destroy_partner');
//set partner status off
Route::put('/admin/off_partner/{partner}', 'PartnerController@off_status');
//set partner status on
Route::put('/admin/on_partner/{partner}', 'PartnerController@on_status');
//get partner view id
Route::get('/admin/partner/{partner}', 'PartnerController@partner_id');
//update partner
Route::put('/admin/partner-update/{partner}', 'PartnerController@update_partner');

//Team view
Route::get('/admin/team', 'TeamController@team');
//add member
Route::post('/admin/add_member', 'TeamController@add_member');
//delete member
Route::get('/destroy_member/{team}', 'TeamController@destroy_member');
//set status off
Route::put('/admin/off_member/{team}', 'TeamController@off_status');
//set status off
Route::put('/admin/on_member/{team}', 'TeamController@on_status');
//get member view id
Route::get('/admin/member/{team}', 'TeamController@member_id');
//update member
Route::put('/admin/member-update/{team}', 'TeamController@update_member');

//News view
Route::get('/admin/news', 'NewsController@news');
//add news
Route::post('/admin/add_news', 'NewsController@add_news');
//delete news
Route::get('/destroy_news/{news}', 'NewsController@destroy_news');
//set news status off
Route::put('/admin/off_news/{news}', 'NewsController@off_status');
//set news status on
Route::put('/admin/on_news/{news}', 'NewsController@on_status');
//get news view id
Route::get('/admin/news/{news}', 'NewsController@news_id');
//update news
Route::put('/admin/news-update/{news}', 'NewsController@update_news');
